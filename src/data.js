{
    "products"  [
    {
        "_id" : 1,
        "prodcutName" : "Laptop",
        "price" : "50000INR",
        "size" : "14.8inches",
        "image" : "/images/laptop.jpg",
        "availableQuantity" : "3",
        "properties" : [{
            "weight" : "600gms",
            "color" : "grey-black"
        }]
    },
    {
        "_id" : 2,
        "prodcutName" : "Samsung Mobile",
        "price" : "15000INR",
        "size" : "6.2inches",
        "image" : "/images/samsung.jpg",
        "availableQuantity" : "2",
        "properties" : [{
            "weight" : "120gms",
            "color" : "gold"
        }]
    },
    {   "_id" : 3,
        "prodcutName" : "Sandles for girls",
        "price" : "400INR",
        "size" : "15cm",
        "image" : "/images/sandles.jpg",
        "availableQuantity" : "1",
        "properties" : [{
            "weight" : "270gms",
            "color" : "Silver"
        }]
    },
    {
        "_id" : 4,
        "prodcutName" : "Laptop",
        "price" : "50000INR",
        "size" : "14.2 inches",
        "image" : "/images/laptop.jpg",
        "availableQuantity" : "3",
        "properties" : [{
            "weight" : "600gms",
            "color" : "grey-black"
        }]
    },
    {
        "_id" : 5,
        "prodcutName" : "Samsung Mobile",
        "price" : "15000INR",
        "size" : "6.2inches",
        "image" : "/images/samsung.jpg",
        "availableQuantity" : "2",
        "properties" : [{
            "weight" : "120gms",
            "color" : "gold"
        }]
    },
    {
        "_id" : 6,
        "prodcutName" : "Sandles for girls",
        "price" : "400INR",
        "size" : "15cm",
        "image" : "/images/sandles.jpg",
        "availableQuantity" : "1",
        "properties" : [{
            "weight" : "270gms",
            "color" : "Silver"
        }]
    }
]
}