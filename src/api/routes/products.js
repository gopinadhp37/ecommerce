const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Product = require('../models/Product');
const Admin = require('../middleware/Admin');
const auth = require('../middleware/auth');

router.get('/', (req, res, next) => {
    Product.find()
        .exec()
        .then(products =>{
            res.status(200).json(products);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        user: req.body.userInfo._id,
        productName : req.body.productName,
        price : req.body.price,
        size : req.body.size,
        image : req.body.image,
        availableQuantity : req.body.availableQuantity,
    })
    product.save()
    .then(result =>{

    })
    res.status(200).json({
        message :'post',
        createdProduct : product
    })
});

router.get("/:productId", (req, res, next) => {
    const id = req.params.productId;
    Product.findById(id)
        .exec()
        .then(product =>{
            console.log(product);
            if(product){
                res.status(200).json(product);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.put('/:id',async (req, res) => {
    const productId = req.params.id;
    const product = await Product.findById(productId);
    if (product) {
      product.productName = req.body.productName;
      product.price = req.body.price;
      product.image = req.body.image;
      product.size = req.body.size;
      product.availableQuantity = req.body.availableQuantity;
      const updatedProduct = await product.save();
      if (updatedProduct) {
        return res
          .status(200)
          .send({ message: 'Product Updated', data: updatedProduct });
      }
    }
    return res.status(500).send({ message: ' Error in Updating Product.' });
  });

router.delete('/:productId', (req, res, next) => {
    const id = req.params.productId;
    Product.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});


module.exports = router;
