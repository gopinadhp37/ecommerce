const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken'); 
const User = require('../models/User');
//const Admin = require('../middleware/Admin');
const isAuth = require('../middleware/auth');

router.get('/createadmin', async (req, res) => {
    try {
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        userName: 'Pasupuleti Gopi',
        email: 'gopinadhpcs37@gmail.com',
        password: 'Warnwill@409',
        contact : "6302990829",
        address: "11-63, pallapu street, Amaravthi",
        isAdmin: true,
      });
      const newUser = await user.save();
      res.send(newUser);
  }catch(error){
    res.send({ message: error.message });
  }});

router.get('/', (req, res) => {
    User.find()
        .exec()
        .then(users =>{
            console.log(users); 
            res.status(200).json(users);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', async (req, res) => {
    const user = new User({
        _id: new mongoose.Types.ObjectId(),
        userName : req.body.userName,
        email : req.body.email,
        password : req.body.password,
        contact : req.body.contact,
        address : req.body.address,
    })
    user.save()
    const token = generateAuthToken(user);
    res.status(200).json({  _id: user._id,
      userName: user.userName,
      email: user.email,
      password:user.password,
      concat:user.contact,
      address: user.address,
      isAdmin: user.isAdmin,
      token: token, })
s});

router.post("/login", async(req, res) => {
    try{
        const user = await findByCredentials(req.body.email, req.body.password);
        const token = await generateAuthToken(user);
        console.log(token, "token");
        if (user) {
            res.send({
              _id: user._id,
              userName: user.userName,
              email: user.email,
              password:user.password,
              isAdmin: user.isAdmin,
              token:  token,
            });
          } else {
            res.status(401).send({ message: 'Invalid Email or Password.' });
          }
        
    }catch(e){
        console.log(e);
        res.status(400).send()
    }
})

router.get("/:userId",(req, res, next) => {
    const id = req.params.userId;
    User.findById(id)
        .exec()
        .then(user =>{
            console.log(user);
            if(user){
                res.status(200).json(user);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.put('/:id', async (req, res) => {
    const userId = req.params.id;
    const user = await User.findById(userId);
    if (user) {
      user.userName = req.body.userName || user.userName;
      user.email = req.body.email || user.email;
      user.password = req.body.password || user.password;
      const updatedUser = await user.save();
      res.send({
        _id: updatedUser.id,
        userName: updatedUser.userName,
        email: updatedUser.email,
        password : updatedUser.password,
        isAdmin: updatedUser.isAdmin,
        token: generateAuthToken(updatedUser),
      });
    } else {
      res.status(404).send({ message: 'User Not Found' });
    }
  });

router.delete('/:userId', (req, res, next) => {
    const id = req.params.userId;
    console.log(id);
    User.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});


findByCredentials = async (email, password) => {
    const user = await User.findOne({email})
    if(!user){
        throw new Error('unable to login');
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if(!isMatch){
        throw new Error('unable to login');
    }
    return user;
};

generateAuthToken = async function(user){
  const token = jwt.sign({_id : user._id.toString() }, 'user',{expiresIn:'36h'});
  user.tokens = user.tokens.concat({token})  
  user.save();
  return token;
}

module.exports = router;
