const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    productName : {
        type : String,
        required : true,
        minlength : 3
    },
    price : {
        type : Number,
        required : true,
    },
    size : {
        type : Number,
        required : true,
    },
    image : {
        type : String,
        required : true
    },
    availableQuantity : {
        type : Number,
        required : true
    },
    isChecked : { type : Boolean, required: true, default: false},
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required:true},
})

module.exports = mongoose.model('Product', productSchema);