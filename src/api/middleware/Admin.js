const Admin = (req, res, next) => {
    console.log(req.user);
    if (req.user && req.user.isAdmin) {
        console.log(req.user, req.user.isAdmin," Admin Section");
      return next();
    }
    return res.status(401).send({ message: 'Admin Token is not valid.' });
  };

  module.exports = Admin;